/* (c) Magnus Auvinen. See licence.txt in the root of the distribution for more information. */
/* If you are missing that file, acquire a complete release at teeworlds.com.                */
/* Modified by unsigned char* */
#include "detect.h"

#if defined(__cplusplus)
extern "C" {
#endif

#ifdef __GNUC__
#define GNUC_ATTRIBUTE(x) __attribute__(x)
#else
#define GNUC_ATTRIBUTE(x)
#endif

/*
	Function: fs_is_dir
		Checks if directory exists

	Returns:
		Returns 1 on success, 0 on failure.
*/
int fs_is_dir(const char *path);

/*
	Function: fs_is_file
		Check if file exists

	Returns:
		Returns 1 on success, 0 on failure.
*/
int fs_is_file(const char *filename);

/*
	Function: str_format
		Performs printf formatting into a buffer.
	Parameters:
		buffer - Pointer to the buffer to receive the formatted string.
		buffer_size - Size of the buffer.
		format - printf formatting string.
		... - Parameters for the formatting.
	Remarks:
		- See the C manual for syntax for the printf formatting string.
		- The strings are treated as zero-termineted strings.
		- Guarantees that dst string will contain zero-termination.
*/
void str_format(char *buffer, int buffer_size, const char *format, ...)
GNUC_ATTRIBUTE((format(printf, 3, 4)));

#if defined(__cplusplus)
}
#endif
