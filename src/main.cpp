/*
 * TWMapImagesRecovery is a tool for extract png images from teeworlds maps.
 *
 * Options:
 *    --version			Shows Version
 *    -m                Map
 *    -d                Destination folder
 *    -v				Verbose Mode
 *    -i				Only shows the info of a map (Version, Author, License, etc..)... use with (-v)
 *    -r				Only recovery the images
 *
 * Default:
 *   By default the application starts in SILENCE mode.
 */
#include "datafile.h"
#include "system.h"
#include <pnglite.h>
#include <iostream>
#define APP_VERSION		"1.7"

enum {
	MODE_SILENCE = 0,
	MODE_VERBOSE,

	OPTION_INFO = 1<<1,
	OPTION_STEAL = 1<<2,
};
bool stealIt(const char *pathMap, const char *pathSave, int options);

short g_Mode = MODE_SILENCE;

int main(int argc, char* argv[])
{
	int options = 0;
	char fileMap[512] = {0};
	char destFolder[512] = {0};

	for (int i=0; i<argc; ++i)
	{
		if (argv[i][0] == '-')
		{
			if (argv[i][1] == 'v')
				g_Mode = MODE_VERBOSE;
			else if (argv[i][1] == 'i')
				options |= OPTION_INFO;
			else if (argv[i][1] == 'r')
				options |= OPTION_STEAL;
			else if (argv[i][1] == 'm')
				strncpy(fileMap, argv[i+1], sizeof(fileMap)-1);
			else if (argv[i][1] == 'd')
				strncpy(destFolder, argv[i+1], sizeof(destFolder)-1);
 			else if (strcmp(argv[i], "--version") == 0)
			{
				std::cout << "TWMapImagesRecovery " << APP_VERSION << std::endl;
				return 0;
			}
		}
	}

	if (!fileMap[0])
	{
		std::string appName = argv[0];
		appName = appName.substr(appName.find_last_of("\\/")+1).c_str();
		std::cout << "Map don't specified. Aborting!" << std::endl << std::endl;
		std::cout << "Usage: " << appName << " [-v][-i][-r][--version][-d destination folder] <-m map>" << std::endl;
		return -1;
	}
	if (!destFolder[0])
		destFolder[0] = '.';

	if (!fs_is_file(fileMap))
	{
		std::cout << "ERROR: The map doesn't exists!" << std::endl << std::endl;
		return -1;
	}

	if (!fs_is_dir(destFolder))
	{
		std::cout << "ERROR: The folder destination doesn't exists!" << std::endl << std::endl;
		return -1;
	}

	png_init(0, 0);

	return stealIt(fileMap, destFolder, options)?0:1;
}

bool stealIt(const char *pathMap, const char *pathSave, int options)
{
	CDataFileReader fileReader;

	if (!fileReader.open(pathMap))
	{
		std::cout << "ERROR: The file isn't a valid map!" << std::endl << std::endl;
		return false;
	}


	// check version
	CMapItemVersion *pItem = (CMapItemVersion *)fileReader.findItem(MAPITEMTYPE_VERSION, 0);
	if (pItem && pItem->m_Version == 1)
	{
		// load map info
		if ((options&OPTION_INFO) || options == OPTION_INFO || (!options && g_Mode == MODE_VERBOSE))
		{
			CMapItemInfo *pItem = (CMapItemInfo *)fileReader.findItem(MAPITEMTYPE_INFO, 0);
			std::cout << "MAP INFO" << std::endl << "====================" << std::endl;
			std::cout << "Author: " << ((pItem->m_Author > -1)?(char *)fileReader.getData(pItem->m_Author):"Unknown") << std::endl;
			std::cout << "Version: " << ((pItem->m_MapVersion > -1)?(char *)fileReader.getData(pItem->m_MapVersion):"Unknown") << std::endl;
			std::cout << "Credits: " << ((pItem->m_Credits > -1)?(char *)fileReader.getData(pItem->m_Credits):"Unknown") << std::endl;
			std::cout << "License: " << ((pItem->m_License > -1)?(char *)fileReader.getData(pItem->m_License):"Unknown") << std::endl;
			std::cout << "====================" << std::endl;
		}

		// load images
		if (!options || (options&OPTION_STEAL))
		{
			int Start, Num;
			fileReader.getType( MAPITEMTYPE_IMAGE, &Start, &Num);

			if (g_Mode == MODE_VERBOSE)
				std::cout << std::endl << "SCANNING " << Num << " IMAGES..." << std::endl << "==========================" << std::endl;

			for(int i = 0; i < Num; i++)
			{
				CMapItemImage *pItem = (CMapItemImage *)fileReader.getItem(Start+i, 0, 0);
				char *pName = (char *)fileReader.getData(pItem->m_ImageName);

				if(pItem->m_External)
				{
					if (g_Mode == MODE_VERBOSE)
						std::cout << "[EXTERNAL] " << pName << " - OMITTED" << std::endl;
				}
				else
				{
					char finalPath[512]={0};
					snprintf(finalPath, sizeof(finalPath), "%s/%s.png", pathSave, pName);

					// copy image data
					png_t png;
					png_open_file_write(&png, finalPath);
					png_set_data(&png, pItem->m_Width, pItem->m_Height, 8, PNG_TRUECOLOR_ALPHA, (unsigned char*)fileReader.getData(pItem->m_ImageData));
					png_close_file(&png);

					if (g_Mode == MODE_VERBOSE)
						std::cout << "[INTERNAL] " << pName << " - EXTRACTED" << std::endl;
				}
			}

			if (g_Mode == MODE_VERBOSE)
				std::cout << "==========================" << std::endl;
		}
	}

	return fileReader.close();
}
