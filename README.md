<h1 align="center">
  <div>TW Map Images Recovery</div>
</h1>

Tool to recover the images that you lost!

More info: https://www.teeworlds.com/forum/viewtopic.php?id=9995

## USAGE

| Short | Long | Description | Required |
|-|-|-|-|
| -m | | Map | Yes |
| -d | | Destination Folder | No |
| -v | | Enable verbose mode | No |
| -i | | Only print information | No | 
| -r | | Only recovery images | No |
| | --version | Prints version | No |

```TWMapImagesRecovery [-v][-i][-r][--version][-d destination folder] <-m map>```

Example: ```TWMapImagesRecovery -v -m mymap.map```


## BUILD PROJECT

#### LINUX

- Install: cmake gcc
- Create a new folder 'build': ```mkdir build```
- Run cmake: ```cmake -DCMAKE_BUILD_TYPE:STRING=Release ..```
- Run make: ```make```

#### WINDOWS

- Download cmake: https://cmake.org/download/
- Download 'Visual Studio 2019 Community Edition': https://visualstudio.microsoft.com/es/downloads/
   - Enable C++ support

- Open cmake
   - Set 'Where is the source code:'
   - Set 'Where to build the binaries:' -> Use a new folder to have a clean build
   - Press 'Configure': Use 'Visual Studio 2019'
   - Press 'Generate'
- Open a new terminal
   - Type: ```%comspec% /k "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"```
   - Type: ```msbuild TWMapImagesRecovery.sln```